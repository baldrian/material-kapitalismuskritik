# Bildung zu Kapitalismuskritik
## Attac: Kapitalismus – oder was? Über Marktwirtschaft und Alternativen
> Das vorliegende Bildungsmaterial »Kapitalismus – oder was? Über Marktwirtschaft und Alternativen« versucht die Lücke zu schließen, die der Bielefelder Professor für Didaktik der Sozialwissenschaften und Wirtschaftssoziologie beschreibt. Es ist so konzipiert, dass es an Lehrplanthemen wie »Soziale Marktwirtschaft« oder Marktformen anschließt, zugleich aber grundsätzliche Fragen zur Funktionsweise des Kapitalismus aufwirft und den Fokus auf die politische Gestaltbarkeit der Wirtschaftsordnung legt.

> Das Material ist in vier Module gegliedert, die jeweils mit einer kurzen fachlichen Einführung und einem knappen didaktischen Kommentar zu den einzelnen Elementen beginnen.

https://www.attac.de/bildungsangebot/bildungsmaterial/sekundarstufe-und-erwachsene/material-kapitalismus

## Rosalux: Bildung zu Kapitalismus und Kapitalismuskritik. Methoden, Fallstricke, Rezensionen, Texte 

https://www.rosalux.de/publikation/id/40444/bildung-zu-kapitalismus-und-kapitalismuskritik

## Rosalux: Marx für Alle!

Ein Tagesseminar zur Einführung in Marx’ Analyse und Kritik des Kapitalismus.
https://www.rosalux.de/publikation/id/39693/marx-fuer-alle/

## Rosalux: Jenseits der Prekarität. Methodensammlung für die Bildungsarbeit 
Die Broschüre «Jenseits der Prekarität. Materialien für politische Bildung und linke Politik» thematisiert die Prekarisierung immer größer werdender Teile der Bevölkerung, deren Ursachen und Auswirkungen. Am Ende findet ihr Vorschläge für Strategien und Forderungen im Kampf gegen Prekarisierung. Ihr könnt euch mithilfe der Broschüre in das Thema einarbeiten oder Auszüge davon als Material für die inhaltliche Arbeit in der Veranstaltung benutzen. 

https://www.rosalux.de/publikation/id/9064/jenseits-der-prekaritaet-1
